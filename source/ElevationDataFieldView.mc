using Toybox.WatchUi;
using Toybox.Activity;
using Toybox.System;
using Toybox.Math;

class ElevationDataFieldView extends WatchUi.SimpleDataField {
	var debug = true;
    // Set the label of the data field here.
    function initialize() {
        SimpleDataField.initialize();
        label = WatchUi.loadResource(Rez.Strings.fieldLabel);
    }

    // The given info object contains all the current workout
    // information. Calculate a value and return it in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    var first = true;
    var lastHeight = 0;
    var completeAscent = 0;
    function compute(info) {
        // See Activity.Info in the documentation for available information.
        var activityRecording = info.timerState == Activity.TIMER_STATE_ON;
		var actualHeight = Math.round(info.altitude);

		if(activityRecording) {
			if(!first) {
				if(actualHeight > lastHeight) {
					var difference = actualHeight - lastHeight;
					completeAscent += difference;
				}
			} else {
				first = false;
				lastHeight = actualHeight;
			}

			if(debug) {
				System.println("Last Height:  " + lastHeight);
				System.println("Actual Height:" + actualHeight);
				System.println("Difference:" + (actualHeight - lastHeight));
				System.println("Ascent:" + completeAscent);
			}
		}

		lastHeight = actualHeight;
        return Math.round(completeAscent);
    }

}